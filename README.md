# README #

The project is a CLI application web crawler. By building a related tree of links it then recursively walks through the tree and collects whatever info is set in the callback. Such info could be counting a quantity of tags on each page in the tree.

### Requirements ###

* Make sure PHP 5.6 is installed or a newer version

### Setup ###

* Clone the project to an executable directory `git clone https://utidevid@bitbucket.org/utidevid/web-crawler.git`

### Usage ###

* Run `php bin/crawler.php url=http://onepagerapp.com/` to start crawling the site.
* Run `php bin/crawler.php url=http://onepagerapp.com/ depth=1` to crawl with a custom depth. The more the depth is set, the more links will be planted in the tree.
    * By default, the depth parameters is always set to 1, which means the site URL and URLs found on the first page of the site.
    * Depth set to 2 will crawl through each URL found on the site.
    * Depth set to 3 will crawl through each URL found on each URL, and so on, depending on the depth
    
* Run `php bin/crawler.php url=http://onepagerapp.com/ limit=5` to limit up to N URLs to crawl (useful with large depths)
* Run `php bin/crawler.php url=http://onepagerapp.com/ crawlMethod=single-page` to crawl only through a single URL, such as main website only
    * Since only one URL is being crawled, depth is permanently disabled
    * Limit is useless in this mode
* Run `php bin/crawler.php url=http://onepagerapp.com/ crawlMethod=web` to crawl through external links, beyond the website domain, too
    * Depth is allowed in this mode
    * Limit is allowed in this mode
* Add `debug` parameter to review the dump of the tree in console or at bin/result.txt

### Console runs the following statistics in real time ###
 * `Crawl URLs to retrieve links for the tree` - renders URLs the crawler reached to build the tree
 * `URLs that are expected to be crawled to process data (N)` - renders URLs from the tree the crawler will use to process operations, such as count tags quantity
 * `Crawl through each URL, which has been retrieved earlier, to process data` - renders URLs the crawler reached to process operations
 * `Report generated at: D:\my\projects\web_crawler\src\Acme\Report/../../../reports/report_08_11_2017.html` - renders a path to the HTML report
 * `URLs that could not be crawled (N)` - URLs that could not be crawled:
    * A URL is unreachable within 5 seconds
    * Response code is not within 200 - 299
    * A cURL error
    * A URL leads beyond the main domain scope, unless set otherwise in the crawler
    * A URL has no content
        
### How it works? ###
* A crawler builds a tree of URLs before executing any counting, comparison or other operations
* As soon as a tree is created, a crawler visit each URL in the tree, executes operations, flags it as processed
* If a URL has failed, it is being kicked off into a separate collection
* A crawler will not revisit URLs that have already been visited
* A crawler is configurable by cURL and scans for elements via PHP DOM
* A crawler does not use a pool or workers, it is being recursive

### Contribution guidelines ###

* Writing tests
* Abstraction for reports
* Extending crawler to use services other than cURL
* Code review
* Other guidelines

### Who do I talk to? ###

Should you have any questions or seek further clarifications, please feel free to contact me at andy.zaporozhets@gmail.com