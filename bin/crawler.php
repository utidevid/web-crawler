<?php

use Acme\Crawler\Crawler;
use Acme\Crawler\CrawlerQueue;
use Acme\Crawler\CrawlingUrl;
use Acme\Crawler\Factory\CrawlerFactory;
use Acme\Crawler\Url;
use Acme\CrawlerCommand;
use Acme\Report\HtmlReport;
use Acme\Report\ReportData;
use Acme\Report\ReportGenerator;
use Acme\Report\TagsSummaryReportModel;

require_once __DIR__ . '/../vendor/autoload.php';

//$url = Url::create('https://en.wikipedia.org/wiki/Slot_machine');
//$url = Url::create('http://localhost:8080/site/home.html');
//$url = Url::create('https://www.develic.com/');
//$url = Url::create('https://apps.shopify.com/sitemap');

$crawlerCommand = new CrawlerCommand($argv);
$crawlerCommand->run();
