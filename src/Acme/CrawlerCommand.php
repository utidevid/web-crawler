<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 08-Nov-17
 * Time: 9:21 AM
 */

namespace Acme;


use Acme\Crawler\Crawler;
use Acme\Crawler\Factory\CrawlerFactory;
use Acme\Crawler\Url;
use Acme\Report\HtmlReport;
use Acme\Report\ReportGenerator;
use Acme\Report\TagsSummaryReportModel;

class CrawlerCommand
{
    const CRAWL_SINGLE_PAGE_METHOD = 'single-page';
    const CRAWL_SITE_METHOD = 'site-only';
    const CRAWL_WEB_METHOD = 'web';

    /**
     * @var Crawler
     */
    private $crawler;

    /**
     * @var string
     */
    private $crawlMethod = self::CRAWL_SITE_METHOD;

    /**
     * @var Url
     */
    private $url = null;

    /**
     * @var int
     */
    private $depth = Crawler::DEFAULT_DEPTH;

    /**
     * @var int|null
     */
    private $limit = null;

    /**
     * @var array
     */
    private $data = array();

    private $debugMode = false;

    public function __construct(array $commands)
    {
        array_shift($commands);

        foreach ($commands as $command) {
            $command = explode('=', $command);

            if (count($command) < 1) {
                break;
            }

            $commandName = $command[0];

            if (isset($command[1])) {
                $commandValue = $command[1];
                $this->$commandName($commandValue);
            } else {
                $this->$commandName();
            }
        }
    }

    public function debug()
    {
        $this->debugMode = true;
    }

    public function isDebugEnabled()
    {
        return $this->debugMode;
    }

    public function crawlMethod($crawlMethod)
    {
        $this->crawlMethod = $crawlMethod;
    }

    public function depth($depth)
    {
        $this->depth = $depth;
    }

    public function limit($limit)
    {
        $this->limit = $limit;
    }

    public function url($url)
    {
        $this->url = Url::create($url);
    }

    public function startCrawling()
    {
        print('Crawl URLs to retrieve links for the tree' . PHP_EOL);

        if ($this->crawlMethod == self::CRAWL_SINGLE_PAGE_METHOD) {
            $this->crawler = CrawlerFactory::crawlSinglePage($this->url, $this->limit);
        } else if ($this->crawlMethod == self::CRAWL_WEB_METHOD) {
            $this->crawler = CrawlerFactory::crawlWeb($this->url, $this->depth, $this->limit);
        } else if ($this->crawlMethod == self::CRAWL_SITE_METHOD) {
            $this->crawler = CrawlerFactory::crawlSite($this->url, $this->depth, $this->limit);
        }

        $this->crawler->run();
    }

    public function run()
    {
        if ($this->url == null) {
            print('Please enter site URL to start crawling through');
        }

        $this->startCrawling();
        $this->pendingQueuedUrlsInfo();
        $this->collectTags();
        $this->generateReport();
        $this->brokenLinksInfo();
        $this->debugQueuedTree();
    }

    private function pendingQueuedUrlsInfo()
    {
        print(PHP_EOL);

        $pendingQueuedUrls = $this->crawler->getQueueTree(Crawler::LIST_TREE_NORMALIZATION);
        print('URLs that are expected to be crawled to process data ('. count($pendingQueuedUrls) . ')' . PHP_EOL);
        foreach ($pendingQueuedUrls as $url => $data) {
            print($url . PHP_EOL);
        }
    }

    private function generateReport()
    {
        $reportData = array();

        foreach ($this->data as $key => $value) {
            $report = new TagsSummaryReportModel();
            $report->setSiteUrl(Url::create($key));
            $report->setTagsCount(count($value['tags']['img']));
            $report->setElapsedTime(round( $value['time']['elapsed_time_miliseconds'] / 1000, 3));

            $reportData[] = $report;
        }

        // sort report by tags descending
        usort($reportData, function(TagsSummaryReportModel $a, TagsSummaryReportModel $b) {
            return ($a->getTagsCount() < $b->getTagsCount()) ? -1 : 1;
        });

        $htmlReport = new HtmlReport('<img> Tags Summary Report dated: ' . date('d F Y'), $reportData);
        $htmlReport->setHeaderItems(['Site URL', 'Tags Quantity', 'Elapsed Time (seconds)']);
        $htmlReport->setFooterItems(['Total sites visited: {total_sites_visited_quantity}', 'Total Tags Quantity: {total_tags_quantity}', 'Total Time Elapsed: {total_elapsed_time}']);
        $htmlReport->setCallback(function (HtmlReport $htmlReport, TagsSummaryReportModel $item) {
            $htmlReport->incCallbackData('total_tags_quantity', $item->getTagsCount());
            $htmlReport->incCallbackData('total_elapsed_time', $item->getElapsedTime());
            $htmlReport->incCallbackData('total_sites_visited_quantity');
        });

        $reportGenerator = new ReportGenerator($htmlReport, 'report_' . date('d_m_Y'));
        $reportGenerator->generate();

        print(PHP_EOL);
        print('Report generated at: ' . $reportGenerator->getFullPath());
    }

    private function brokenLinksInfo()
    {
        print(PHP_EOL);
        $brokenUrls = $this->crawler->getBrokenUrls();
        if ($brokenUrls != null) {
            print(PHP_EOL);
            print('URLs that could not be crawled (' . count($brokenUrls) . ')' . PHP_EOL);
            foreach ($brokenUrls as $url => $data) {
                print('Reasons for ' . $url . ':' . PHP_EOL);

                foreach ($data['reasons'] as $reason) {
                    print(' - ' . $reason . PHP_EOL);
                }
            }
        }
    }

    private function debugQueuedTree()
    {
        if ($this->isDebugEnabled()) {
            print(PHP_EOL);

            $data = $this->crawler->getQueueTree(Crawler::SIMPLIFY_TREE_NORMALIZATION);
            ob_start();
            var_dump($data);
            $result = ob_get_clean();
            file_put_contents(__DIR__ . '/../../bin/result.txt', $result);
            var_dump($result);
        }
    }

    private function collectTags()
    {
        print(PHP_EOL);
        print('Crawl through each URL, which has been retrieved earlier, to process data' . PHP_EOL);
        $this->data = $this->crawler->collectTags(['img']);
        print(PHP_EOL . 'URLs parsed: ' . count($this->data) . PHP_EOL);
    }

}