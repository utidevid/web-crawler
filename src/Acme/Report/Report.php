<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 07-Nov-17
 * Time: 5:36 PM
 */

namespace Acme\Report;


abstract class Report
{
    const INC_OPERATION = 1;
    const DEC_OPERATION = 2;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var \Closure ($this, AbstractReportModel $item)
     */
    protected $callback;
    private $callbackData = array();

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Build a report in a given format
     * @return mixed
     */
    abstract public function output();

    /**
     * Get report format it is written in, e.g. html, json, xml, txt, array
     * @return string
     */
    abstract public function format();

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    protected function parsePlaceholder($item)
    {
        foreach ($this->callbackData as $placeholder => $value) {
            if (strpos($item, '{' . $placeholder . '}')) {
                return str_replace('{' . $placeholder . '}', $value, $item);
            }
        }
    }

    protected function call($item)
    {
        if ($this->callback instanceof \Closure) {
            $callback = $this->callback;
            $callback($this, $item);
        }
    }

    /**
     * @param \Closure $callback
     */
    public function setCallback(\Closure $callback) {
        $this->callback = $callback;
    }

    /**
     * Put a value
     *
     * @param $key
     * @param $value
     */
    public function putCallbackData($key, $value)
    {
        $this->callbackData[$key] = $value;
    }

    /**
     * Get a value
     *
     * @param $key
     * @return mixed
     */
    public function getCallbackData($key)
    {
        if (!isset($this->callbackData[$key])) {
            $this->callbackData[$key] = null;
        }

        return $this->callbackData[$key];
    }

    /**
     * Increment or decrement a value
     * @param $unary
     * @param $key
     * @param $value
     */
    private function unaryOperation($unary, $key, $value)
    {
        if (!isset($this->callbackData[$key])) {
            $this->callbackData[$key] = 0;
        }

        if ($unary === self::INC_OPERATION) {
            $this->callbackData[$key] += $value;
        } else {
            $this->callbackData[$key] -= $value;
        }
    }

    /**
     * Decrement a value
     *
     * @param $key
     * @param $value
     */
    public function decCallbackData($key, $value = 1)
    {
        $this->unaryOperation(self::DEC_OPERATION, $key, $value);
    }

    /**
     * Increment a value
     *
     * @param $key
     * @param $value
     */
    public function incCallbackData($key, $value = 1)
    {
        $this->unaryOperation(self::INC_OPERATION, $key, $value);
    }
}