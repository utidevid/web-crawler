<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 07-Nov-17
 * Time: 5:51 PM
 */

namespace Acme\Report;


class HtmlReport extends Report
{
    protected $headerItems = array();
    protected $footerItems = array();
    /**
     * @var array
     */
    private $title;

    public function __construct($title, array $data)
    {
        parent::__construct($data);
        $this->title = htmlentities($title);
    }

    public function setHeaderItems(array $items)
    {
        $this->headerItems = $items;
    }

    public function hasHeader()
    {
        return $this->headerItems != null;
    }

    public function setFooterItems(array $items)
    {
        $this->footerItems = $items;
    }

    public function hasFooter()
    {
        return $this->footerItems != null;
    }

    /**
     * Build a report in a given format
     * @return mixed
     */
    public function output()
    {
        $html = '<html><head>';
        $html .= '<title>' . $this->title . '</title>';
        $html .= "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css\" integrity=\"sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb\" crossorigin=\"anonymous\">";
        $html .= '</head><body>';

        $html .= '<h1 class="display-4">' . $this->title . '</h1>';
        $html .= '<table id="table" class="table">';

        // Compose table header
        if ($this->hasHeader()) {
            $html .= '<thead><tr>';
            foreach ($this->headerItems as $item) {
                $html .= '<th>' . $item . '</th>';
            }
            $html .= '</tr></thead>';
        }

        $html .= '<tbody>';

        // Compose table body
        /** @var AbstractReportModel $reportModel */
        foreach ($this->data as $reportModel) {
            if ($reportModel instanceof AbstractReportModel) {
                $html .= '<tr>';

                $reportData = $reportModel->toArray();
                foreach ($reportData as $item) {
                    $html .= '<td>' . $item . '</td>';
                }

                $this->call($reportModel);

                $html .= '</tr>';
            }
        }

        $html .= '</tbody>';

        // Compose table footer
        if ($this->hasFooter()) {
            $html .= '<tfoot><tr>';
            foreach ($this->footerItems as $item) {
                $html .= '<td> ' . $this->parsePlaceholder($item) . ' </td>';
            }
            $html .= '</tr></tfoot>';
        }

        $html .= '</table>';
        $html .= '</body></html>';

        return $html;
    }

    /**
     * Get report format it is written in
     *
     * @return string
     */
    public function format()
    {
        return 'html';
    }

    /**
     * @return array
     */
    public function getTitle()
    {
        return $this->title;
    }
}