<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 07-Nov-17
 * Time: 7:28 PM
 */

namespace Acme\Report;


abstract class AbstractReportModel
{
    /**
     * Get model template as array
     * @return array
     */
    abstract public function toArray();
}