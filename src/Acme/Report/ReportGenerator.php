<?php

namespace Acme\Report;


class ReportGenerator
{
    const DEFAULT_DIRECTORY = 'reports';
    const DEFAULT_PATH = __DIR__ . '/../../../' . self::DEFAULT_DIRECTORY;

    /**
     * @var Report
     */
    private $report;

    /**
     * @var string
     */
    private $path;

    private $keepOldReports = false;

    /**
     * @var string
     */
    private $filename;

    /**
     * ReportGenerator constructor.
     * @param Report $report - Report itself
     * @param $filename - file name to save the report to
     * @param string $path - Specific path where to save the report (default is project_root/reports)
     */
    public function __construct(Report $report, $filename, $path = self::DEFAULT_PATH)
    {
        $this->report = $report;
        $this->path = $path;
        $this->filename = $filename;
    }

    /**
     * Determine and get the file extension by report format
     * @return string
     */
    private function getFileExtension()
    {
        $format = $this->report->format();

        if ($format != null) {
            return '.' . $format;
        }

        return '';
    }

    /**
     * Get file name with extension
     * @return string
     */
    public function getFilename()
    {
        return $this->filename. $this->getFileExtension();
    }

    public function getFullPath()
    {
        return rtrim($this->path, '/') . '/' . $this->getFilename();
    }

    /**
     * Keep old reports
     */
    public function keepOldReports()
    {
        $this->keepOldReports = true;
    }

    public function generate()
    {
        if (!file_exists(self::DEFAULT_PATH)) {
            mkdir(self::DEFAULT_PATH, 0777, true);
        }

        file_put_contents(
            $this->getFullPath(),
            $this->report->output()
        );
    }
}