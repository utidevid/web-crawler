<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 04-Nov-17
 * Time: 6:40 PM
 */

namespace Acme\Report;


use Acme\Crawler\Url;

class TagsSummaryReportModel extends AbstractReportModel
{
    /**
     * @var Url
     */
    private $siteUrl;

    /**
     * @var int
     */
    private $tagsCount;

    /**
     * @var double
     */
    private $elapsedTime;

    /**
     * @return Url
     */
    public function getSiteUrl()
    {
        return $this->siteUrl;
    }

    /**
     * @param Url $siteUrl
     */
    public function setSiteUrl(Url $siteUrl)
    {
        $this->siteUrl = $siteUrl;
    }

    /**
     * @return int
     */
    public function getTagsCount()
    {
        return $this->tagsCount;
    }

    /**
     * @param int $tagsCount
     */
    public function setTagsCount($tagsCount)
    {
        $this->tagsCount = $tagsCount;
    }

    /**
     * @return float
     */
    public function getElapsedTime()
    {
        return $this->elapsedTime;
    }

    /**
     * @param float $elapsedTime
     */
    public function setElapsedTime($elapsedTime)
    {
        $this->elapsedTime = $elapsedTime;
    }

    /**
     * Get model template as array
     * @return array
     */
    public function toArray()
    {
        return array(
            'site_url' => (string) $this->siteUrl,
            'tags_quantity' => $this->tagsCount,
            'elapsed_time' => $this->elapsedTime,
        );
    }
}