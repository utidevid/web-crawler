<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 08-Nov-17
 * Time: 8:30 AM
 */

namespace Acme\Crawler\Factory;


use Acme\Crawler\Crawler;
use Acme\Crawler\CrawlerQueue;
use Acme\Crawler\Url;

class CrawlerFactory
{
    /**
     * Crawl only through a single page
     *
     * @param Url $url
     * @param null $limit
     * @return Crawler
     */
    public static function crawlSinglePage(Url $url, $limit = null)
    {
        return new Crawler($url, new CrawlerQueue(), Crawler::SITE_ONLY_DEPTH, $limit);
    }

    /**
     * Crawl through a site based on depth
     *
     * @param Url $url
     * @param int $depth
     * @param null $limit
     * @return Crawler
     */
    public static function crawlSite(Url $url, $depth = Crawler::DEFAULT_DEPTH, $limit = null)
    {
        return new Crawler($url, new CrawlerQueue(), $depth, $limit);
    }

    /**
     * Crawl through a site based on depth, including external URLs
     *
     * @param Url $url
     * @param int $depth
     * @param null $limit
     * @return Crawler
     */
    public static function crawlWeb(Url $url, $depth = Crawler::DEFAULT_DEPTH, $limit = null)
    {
        $crawler = new Crawler($url, new CrawlerQueue(), $depth, $limit);
        $crawler->enableExternalCrawling();

        return $crawler;
    }
}