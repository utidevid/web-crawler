<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 08-Nov-17
 * Time: 6:25 AM
 */

namespace Acme\Crawler;


abstract class AbstractCrawlerQueue
{
    /**
     * Contains URLs that have already been crawled through
     * @var array
     */
    protected $crawledUrls = array();

    /**
     * Contains URLs that have already been processed
     * @var array
     */
    protected $processedUrls = array();

    /**
     * Contains URLs that could not be reached
     * @var array
     */
    protected $brokenUrls = array();

    /**
     * Flag a crawling url as processed
     *
     * @param CrawlingUrl $crawlingUrl
     */
    public function finishProcessing(CrawlingUrl $crawlingUrl) {
        $this->processedUrls[(string) $crawlingUrl->getUrl()] = $crawlingUrl;
    }

    /**
     * Is a crawling url done processing
     *
     * @param CrawlingUrl $crawlingUrl
     * @return bool
     */
    public function isProcessed(CrawlingUrl $crawlingUrl)
    {
        if (array_key_exists((string) $crawlingUrl->getUrl(),$this->processedUrls)) {
            return true;
        }

        return false;
    }

    /**
     * Push a crawling URL to the queue tree
     *
     * @param CrawlingUrl $crawlingUrl
     */
    abstract function push(CrawlingUrl $crawlingUrl);

    /**
     * Get the queue tree of crawling URLs
     * @return array
     */
    abstract function getQueueTree();

    /**
     * Get parent children of recently added crawling URL to the queue
     *
     * @return array
     */
    abstract function getLastTreeChildren();

    /**
     * Clean the collection of processed URL after operations on them are done
     *
     * @return void
     */
    public function cleanProcessedUrls()
    {
        $this->processedUrls = array();
    }

    /**
     * Get URLs that could not be crawled
     *
     * @return array
     */
    public function getBrokenUrls()
    {
        return $this->brokenUrls;
    }

    /**
     * Kick a URL from processing the crawler fails to reach it
     *
     * @param CrawlingUrl $crawlingUrl
     * @param $reason - why URL failed
     * @return void
     */
    public function kick(CrawlingUrl $crawlingUrl, $reason)
    {
        $brokenUrl = &$this->brokenUrls[(string) $crawlingUrl->getUrl()];

        if (!isset($brokenUrl['reasons'])) {
            $brokenUrl['reasons'] = array();
        }

        if (!in_array($reason, $brokenUrl['reasons'])) {
            $brokenUrl['reasons'][] = $reason;
        }
    }

    /**
     * Flag a URL as crawled
     *
     * @param CrawlingUrl $crawlingUrl
     * @return void
     */
    public function crawled(CrawlingUrl $crawlingUrl)
    {
        $this->crawledUrls[] = (string) $crawlingUrl->getUrl();
    }

    /**
     * Check if URL has already been crawled
     *
     * @param CrawlingUrl $crawlingUrl
     * @return bool
     */
    public function isCrawled(CrawlingUrl $crawlingUrl)
    {
        $url = (string) $crawlingUrl->getUrl();

        // URL has been crawled by checking in the collection
        if (in_array($url, $this->crawledUrls)) {
            return true;
        }

        // URL is always considered to be crawled if it is found in the broken URLs collection, too
        if (array_key_exists($url, $this->brokenUrls)) {
            return true;
        }

        return false;
    }

    /**
     * Get URLs that have already been crawled
     *
     * @return array
     */
    public function getCrawledUrls()
    {
        return $this->crawledUrls;
    }
}