<?php

namespace Acme\Crawler;


class Url
{
    /**
     * @var null|string
     */
    private $scheme;

    /**
     * @var null|string
     */
    private $host;

    /**
     * @var null|string
     */
    private $path;

    /**
     * @var null|int
     */
    private $port;

    /**
     * @var null|string
     */
    private $query;

    public function __construct($url)
    {
        $urlParts = parse_url($url);

        $this->scheme = $urlParts['scheme'];
        $this->host = $urlParts['host'];
        $this->path = isset($urlParts['path']) ? $urlParts['path'] : null;
        $this->port = isset($urlParts['port']) ? $urlParts['port'] : null;
        $this->query = isset($urlParts['query']) ? $urlParts['query'] : null;
    }

    /**
     * @param string $url - the actual value of href attribute
     * @param Url $originalUrl - original URL requested if some parts of $url parameter are missing
     * @return static
     * @throws \Exception - in case the $originalUrl is not a valid URL
     */
    public static function create($url, Url $originalUrl = null)
    {
        // If href is a path, then build a link based on the url
        if (strpos($url, 'http') !== 0) {
            $path = '/' . ltrim($url, '/');
            $url = $originalUrl->getScheme() . '://';
            $url .= $originalUrl->getHost();
            $url .= $path;
        }

        return new static($url);
    }

    public function isUrl()
    {
        return filter_var($this->rawUrl, FILTER_VALIDATE_URL);
    }

    private function startsWith($path, $needle)
    {
        if ($needle != '' && substr($path, 0, strlen($needle)) === (string) $needle) {
            return true;
        }

        return false;
    }

    public function getUrl()
    {
        $path = $this->startsWith($this->path, '/') ? substr($this->path, 1) : $this->path;
        $port = ($this->port === null ? '' : ":{$this->port}");
        $queryString = ($this->query === null) ? '' : "?{$this->query}";
        return $this->scheme . '://' . $this->host . $port . '/' . $path . $queryString;
    }

    public function isEqual(Url $url)
    {
        return (string) $this === (string) $url;
    }

    public function __toString()
    {
        return $this->getUrl();
    }

    /**
     * @return null|string
     */
    public function getScheme()
    {
        return $this->scheme;
    }

    /**
     * @param null|string $scheme
     */
    public function setScheme($scheme)
    {
        $this->scheme = $scheme;
    }

    /**
     * @return null|string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param null|string $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return null|string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param null|string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return null|int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param int $port
     */
    public function setPort($port)
    {
        $this->port = $port;
    }

    /**
     * @return null|string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param null|string $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }
}