<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 04-Nov-17
 * Time: 8:37 PM
 */

namespace Acme\Crawler;


class CrawlingUrl
{
    /**
     * @var Url
     */
    private $url;

    /**
     * @var Url - the url it was found on
     */
    private $parent;

    public function __construct(Url $url, CrawlingUrl $parent = null)
    {
        $this->url = $url;
        $this->parent = $parent;
    }

    /**
     * Create a crawling url
     *
     * @param Url $url
     * @param CrawlingUrl|null $parent
     * @return static
     */
    public static function create(Url $url, CrawlingUrl $parent = null)
    {
        return new static($url, $parent);
    }

    /**
     * Create a crawling url from string
     *
     * @param $url
     * @param CrawlingUrl|null $parent
     * @return static
     */
    public static function createFromString($url, CrawlingUrl $parent = null)
    {
        return new static(Url::create($url), $parent);
    }

    /**
     * @return Url
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return bool
     */
    public function hasParent()
    {
        if ($this->parent !== null) {
            return true;
        }

        return false;
    }

    /**
     * @return CrawlingUrl|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    public function isEqual(CrawlingUrl $crawlingUrl)
    {
        return (string) $this->getUrl() == (string) $crawlingUrl->getUrl();
    }

    /**
     * @return CrawlingUrl|null
     */
    public function getRoot()
    {
        $root = func_num_args() > 0 ? func_get_arg(0) : null;

        if ($root === null && $this->hasParent()) {
            $root = $this;
        }

        if ($root instanceof $this && $root->hasParent()) {
            return $this->getRoot($root->getParent());
        }

        return $root;
    }

    /**
     * Prevents parent and child URLs being stuck in the recursion until the depth is queue satisfied
     *
     * @return bool
     */
    public function isRepeated()
    {
        // Super parent (parent of the parent) equals this
        if ($this->hasParent()) {
            $parent = $this->getParent();

            if ($parent->hasParent() && $parent->getParent()->isEqual($this)) {
                return true;
            }
        }

        // This has a child referencing the root element
        if ($this->hasParent() && $this->isEqual($this->getRoot())) {
            return true;
        }

        return false;
    }
}