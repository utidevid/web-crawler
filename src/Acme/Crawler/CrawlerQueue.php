<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 05-Nov-17
 * Time: 5:05 PM
 */

namespace Acme\Crawler;


class CrawlerQueue extends AbstractCrawlerQueue
{
    /**
     * Contains children of the parent of the last push URL
     * @var array
     */
    protected $lastTreeChildren = array();

    /**
     * A complete tree of URLs with depth
     * @var array
     */
    private $pendingUrls = array();

    /**
     * Get the queue tree of crawling URLs
     * @return array
     */
    function getQueueTree()
    {
        return $this->pendingUrls;
    }

    /**
     * Push a crawling URL to the queue tree
     *
     * @param CrawlingUrl $crawlingUrl
     */
    function push(CrawlingUrl $crawlingUrl)
    {
        $treeItem = $this->recreateTree($crawlingUrl);

        $this->pendingUrls = array_replace_recursive($this->pendingUrls, $treeItem);

        $this->lastTreeChildren = $this->locateInQueue($treeItem, $this->pendingUrls);
    }

    /**
     * Get parent children of recently added crawling URL to the queue
     *
     * @return array
     */
    function getLastTreeChildren()
    {
        return $this->lastTreeChildren;
    }

    private function locateInQueue(array $tree, &$data)
    {
        $treeItemKey = key($tree);
        $children = $tree[$treeItemKey]['children'];

        if ($data[$treeItemKey]['children'] == null) {
            return $data;
        }

        return $this->locateInQueue($children, $data[$treeItemKey]['children']);
    }

    /**
     * Recreate a complete crawling url tree from a child item
     *
     * @param CrawlingUrl $crawlingUrl
     * @param array $treeItem
     * @return array
     */
    private function recreateTree(CrawlingUrl $crawlingUrl, array $treeItem = array())
    {
        $treeItem = array(
            (string)$crawlingUrl->getUrl() => [
                'url' => $crawlingUrl,
                'children' => $treeItem
            ]
        );

        if ($crawlingUrl->hasParent()) {
            return $this->recreateTree($crawlingUrl->getParent(), $treeItem);
        }

        return $treeItem;
    }
}