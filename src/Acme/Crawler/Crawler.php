<?php

namespace Acme\Crawler;

use DOMDocument;
use DOMNodeList;

/**
 * Class Crawler
 * The core class responsible for crawling through a site and collecting HTML tags
 */
class Crawler
{
    const DOM_DOCUMENT_VERSION = '1.0';

    const DEFAULT_DEPTH = 1; // crawl through site and its links
    const SITE_ONLY_DEPTH = 0; // crawl through site only

    const DEFAULT_LIMIT = null; // null for "No limit"

    const ORIGINAL_TREE_NORMALIZATION = 1;
    const NORMALIZE_TREE_NORMALIZATION = 2;
    const SIMPLIFY_TREE_NORMALIZATION = 3;
    const LIST_TREE_NORMALIZATION = 4;

    /**
     * @var int depth of pages to crawl
     */
    private $depth;

    /**
     * @var AbstractCrawlerQueue
     */
    private $crawlerQueue;
    /**
     * @var Url $siteUrl - a primary website url to start crawling through
     */
    private $siteUrl;
    /**
     * @var int
     */
    private $limit;

    /**
     * @var array
     */
    private $collectedTags;

    /**
     * @var bool determines whether external links are crawled
     */
    private $externalCrawling = false;

    public function __construct(Url $siteUrl, AbstractCrawlerQueue $crawlerQueue, $depth = self::DEFAULT_DEPTH, $limit = self::DEFAULT_LIMIT)
    {
        $this->depth = $depth;
        $this->crawlerQueue = $crawlerQueue;
        $this->siteUrl = $siteUrl;
        $this->limit = $limit;
    }

    /**
     * Run crawler
     */
    public function run()
    {
        $this->initializeCrawlerQueue();
    }

    /**
     * Initialize correct crawling method
     */
    private function initializeCrawlerQueue()
    {
        // Add only one link to the queue to process only this page
        if ($this->depth === self::SITE_ONLY_DEPTH) {
            $rootCrawlingUrl = CrawlingUrl::createFromString($this->siteUrl);
            $this->crawlerQueue->push($rootCrawlingUrl);
            $this->crawlerQueue->crawled($rootCrawlingUrl);
        } else { // Crawl through the page and collect its links
            $this->prepareCrawlerQueue();
        }
    }

    /**
     * Prepare crawler queue by filling it up with URLs on which the further operations will be done
     *
     * @param array $queueData
     * @param int $currentDepth
     */
    private function prepareCrawlerQueue(array $queueData = array(), $currentDepth = 1)
    {
        // Initialize the crawler from inside by visiting the website
        if ($currentDepth === 1) {
            $rootCrawlingUrl = CrawlingUrl::createFromString($this->siteUrl);

            $this->crawlerQueue->push($rootCrawlingUrl);
            $this->crawl($rootCrawlingUrl, $this->parseUrls());
            $this->crawlerQueue->crawled($rootCrawlingUrl);

            $rootUrls = $this->crawlerQueue->getQueueTree()[(string) $this->siteUrl]['children'];

            $this->prepareCrawlerQueue($rootUrls,$currentDepth + 1);
        }

        // Once crawler has visited the site and depth enables crawler to visit links, then crawl through them
        if ($currentDepth <= $this->depth) {
            foreach ($queueData as $queuedUrl) {
                /** @var CrawlingUrl $crawlUrl */
                $crawlUrl = $queuedUrl['url'];

                // Disallow repeated links conditionally or already crawled pages to avoid redundant tree branches
                if ($crawlUrl->isRepeated() || $this->crawlerQueue->isCrawled($crawlUrl)) {
                    continue;
                }

                $this->crawl($crawlUrl, $this->parseUrls());

                // Flag URL as crawled
                $this->crawlerQueue->crawled($crawlUrl);

                // Crawl through children of the tree
                $children = $this->crawlerQueue->getLastTreeChildren();

                if ($children != null) {
                    $this->prepareCrawlerQueue($children, $currentDepth + 1);
                }
            }
        }
    }

    /**
     * Parse and save URLs to the queue
     * 
     * @return \Closure
     */
    private function parseUrls() {
        /**
         * @param DOMDocument $dom
         * @param CrawlingUrl $crawlingUrl
         */
        return function (DOMDocument $dom, CrawlingUrl $crawlingUrl) {
            /** @var DOMNodeList $elements */
            $elements = $dom->getElementsByTagName('a');

            /** @var \DOMElement $element */
            foreach ($elements as $element) {
                if (!$element->hasAttribute('href')) {
                    continue;
                }

                $href = $element->getAttribute('href');

                // A crawling URL is required to maintain the hierarchy of the tree
                $parsedCrawlingUrl = CrawlingUrl::create(
                    Url::create($href, $crawlingUrl->getUrl()),
                    $crawlingUrl
                );


                // Push the crawling url to the queue
                $this->crawlerQueue->push($parsedCrawlingUrl);

                // The limitation feature works against the quantity of queued items, not elements parsed
                // Otherwise, a non-conforming or repeated URLs are counted for the parse
                $urlsQuantity = count($this->crawlerQueue->getLastTreeChildren());

                // Quit crawling if the limit was bent
                if ($this->limit !== null && $urlsQuantity >= $this->limit) {
                    break 1;
                }
            }
        };
    }

    /**
     * Load html data to crawl through
     *
     * @param CrawlingUrl $crawlingUrl
     * @return mixed|null
     */
    private function loadData(CrawlingUrl $crawlingUrl)
    {
        if (!$this->isExternalCrawlingAllowed() && $crawlingUrl->getUrl()->getHost() !== $this->siteUrl->getHost()) {
            $this->crawlerQueue->kick($crawlingUrl, 'External URLs are not crawled');
            return null;
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, (string) $crawlingUrl->getUrl());
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: text/html',
        ));

        $html = curl_exec($ch);
        $error = curl_error($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        // Kick a URL to the broken URLs collection
        if ($error) {
            $this->crawlerQueue->kick($crawlingUrl, $error);
        }

        // Kick a URL if its HTML content was denied by the server
        if (!$html) {
            $this->crawlerQueue->kick($crawlingUrl, 'CURL could not parse content of this URL');
        }

        if($statusCode >= 200 && $statusCode < 300) {
            print((string) $crawlingUrl->getUrl() . ' ' . $statusCode . ' ' . PHP_EOL);
            return $html;
        } else {
            $this->crawlerQueue->kick($crawlingUrl, 'Server responded with ' . $statusCode . ' status code');
        }

        return null;
    }

    /**
     * @param CrawlingUrl|null $crawlingUrl
     * @param \Closure $callback (DOMNodeList $elements)
     */
    protected function crawl(CrawlingUrl $crawlingUrl, $callback)
    {
        $html = $this->loadData($crawlingUrl);

        if ($html) {
            $dom = new DOMDocument(self::DOM_DOCUMENT_VERSION);
            //@$dom->loadHTMLFile((string) $crawlingUrl->getUrl());
            @$dom->loadHTML($html);

            $callback($dom, $crawlingUrl);
        }
    }

    /**
     * Collect the following tags by attributes
     *
     * @param array $tags parse these tags on the page ['tag1', 'tag2', 'tagN']
     * @param array $attrs include the following tags' value to the collection ['tag_name' => ['attr1', 'attr2', 'attrN']]
     * @param array $verifyAttrs collect tag only if one of the elements exists ['tag_name' => ['attr1', 'attr2', 'attrN']]
     *
     * @return array
     */
    public function collectTags(array $tags, array $attrs = array(), array $verifyAttrs = array())
    {
        $collectedTags = array();

        $queuedTree = $this->getQueueTree();

        array_walk_recursive($queuedTree, function ($item) use (&$collectedTags, $tags, $attrs, $verifyAttrs) {
            if ($item instanceof CrawlingUrl && !$this->crawlerQueue->isProcessed($item)) {
                $startTime = microtime(true);

                $this->crawl($item, function(DOMDocument $dom, CrawlingUrl $crawlingUrl) use (&$collectedTags, $startTime, $tags, $attrs, $verifyAttrs) {
                    foreach ($tags as $tag) {
                        /** @var DOMNodeList $elements */
                        $elements = $dom->getElementsByTagName($tag);

                        // Aggregate parsed tags
                        $result = &$collectedTags[(string) $crawlingUrl->getUrl()];
                        $result['time']['elapsed_microtime'] = microtime(true) - $startTime;
                        $result['time']['elapsed_time_miliseconds'] = round($result['time']['elapsed_microtime'] * 1000);

                        $resultTags = &$result['tags'];

                        // Collect elements from the page
                        for ($tagIndex = 0; $tagIndex < $elements->length; $tagIndex++) {
                            /** @var \DOMElement $element */
                            $element = $elements->item($tagIndex);

                            // Verify if the element has required attributes
                            if (isset($verifyAttrs[$tag])) {
                                foreach ($verifyAttrs[$tag] as $attr) {
                                    if (!$element->hasAttribute($attr)) {
                                        continue 2;
                                    }
                                }
                            }

                            $resultTagItem = &$resultTags[$element->nodeName][$tagIndex];
                            $resultTagItem['tag_name'] = $element->tagName;

                            // Collect attributes of the tag
                            if (isset($attrs[$tag])) {
                                foreach ($attrs[$tag] as $attr) {
                                    if ($element->hasAttribute($attr)) {
                                        $resultTagItem[$attr] = $element->getAttribute($attr);
                                    }

                                    if ($attr == 'value') {
                                        $resultTagItem['tag_value'] = $element->nodeValue;
                                    }
                                }
                            }
                        }
                    }

                    $this->crawlerQueue->finishProcessing($crawlingUrl);
                });
            }
        });

        // Clean processed URLs stack
        $this->crawlerQueue->cleanProcessedUrls();

        // Save tags
        $this->collectedTags = $collectedTags;

        return $collectedTags;
    }

    /**
     * Get pending URLs from the queue. By default: the hierarchical structure is preserved
     *
     * @param int $normalization - if true, treat crawling urls as simple URL objects;
     * @return array
     */
    public function getQueueTree($normalization = self::ORIGINAL_TREE_NORMALIZATION)
    {
        $queuedTree = $this->crawlerQueue->getQueueTree();
        $newTree = array();

        if ($normalization > self::ORIGINAL_TREE_NORMALIZATION) {
            array_walk_recursive($queuedTree, function (&$item) use ($normalization, &$newTree) {
                if ($item instanceof CrawlingUrl) {
                    if ($normalization == self::NORMALIZE_TREE_NORMALIZATION) {
                        $item = $item->getUrl();
                    } else if ($normalization == self::SIMPLIFY_TREE_NORMALIZATION) {
                        $item = (string) $item->getUrl();
                    } else if ($normalization == self::LIST_TREE_NORMALIZATION) {
                        $newTree[(string) $item->getUrl()] = (string) $item->getUrl();
                    }
                }
            });
        }

        if ($newTree != null) {
            $queuedTree = $newTree;
        }

        return $queuedTree;
    }

    public function getCrawledUrls()
    {
        return $this->crawlerQueue->getCrawledUrls();
    }

    /**
     * Get URLs could not be reached by the crawler
     *
     * @return array
     */
    public function getBrokenUrls()
    {
        return $this->crawlerQueue->getBrokenUrls();
    }

    /**
     * Are external links crawled
     *
     * @return bool
     */
    public function isExternalCrawlingAllowed()
    {
        return $this->externalCrawling === true;
    }

    /**
     * Crawl external links
     */
    public function enableExternalCrawling()
    {
        $this->externalCrawling = true;
    }
}